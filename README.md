# App-Noticias 

_Aplicacion basica de noticias usando una api free de Newsapi_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋


_Ncesitas Node js , Angular , firebase_
_Ncesitas crear una api en la paginas https://newsapi.org/_


### Instalación 🔧

_ejecutar para tener un entorno de desarrollo ejecutandose_

_Instalar npm_

```
npm install
```
_Generar un key y agregarla en el archivo Environment_
```
apiKey: 'd98f6bd057df4700a53d5eee7a075ac0',
```



### Aplicaciones web progresivas en angular  🔩

_Hacer de su aplicación Angular una PWA_
```
ng add @angular/pwa
```
_Una vez que se haya agregado este paquete, ejecute en el  directorio principal estará listo para implementarse como PWA._
```
ionic build --prod --service-worker
```
## Despliegue en firebase ⚙️
https://prueba-dd822.firebaseapp.com/tabs/tab1 

_una ves generados el buil del pwa procedes subirlo al hosting_
```
firebase deploy
```

## Construido con 🛠️

_Menciona las herramientas que utilizaste para crear tu proyecto_

* [Ionic](https://ionicframework.com/) - El framework usado
* [News Api](https://newsapi.org/) - Api de Notcias
* [Firebase](https://firebase.google.com/) - Usado el hosting


## Actualizaciones 📌
Con disponibilidad de tiempo ire Actualizando

## Autor ✒️

* **Armando chavez** - *Trabajo Inicial* - [chavezm](https://gitlab.com/chavezm)

También puedes mirar la lista de todos los [contribuyentes](https://github.com/your/project/contributors) quíenes han participado en este proyecto. 
# Example screenshots:
<p align="center">
  <img src="https://imgur.com/50MeuNQ.png" width="250" title="hover text">
  <img src="https://imgur.com/if4L71f.png" width="268" alt="accessibility text">
  <img src="https://imgur.com/OlQFW2L.png" width="264" alt="accessibility text">
  <img src="https://imgur.com/sbGBm5P.png" width="264" alt="accessibility text">
  
</p>


## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 o un café ☕ a alguien del equipo. 
* Muchas Gracias  🤓.

---
⌨️ con ❤️ por [Armando Chavez](https://gitlab.com/chavezm) 😊
